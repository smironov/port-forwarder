package ca.mironov.net.pfwd;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

class PortForwarderServer {

    private final Logger logger = Logger.getLogger(getClass().getName());

    private final InetSocketAddress localSocketAddress;
    private final InetSocketAddress remoteSocketAddress;
    private Thread serverThread = null;

    PortForwarderServer(InetSocketAddress localSocketAddress, InetSocketAddress remoteSocketAddress) {
        this.localSocketAddress = localSocketAddress;
        this.remoteSocketAddress = remoteSocketAddress;
    }

    void start() {
        if (serverThread != null && serverThread.isAlive()) {
            throw new IllegalStateException("already started");
        }
        serverThread = new Thread(() -> {
            try {
                ServerSocket serverSocket = new ServerSocket(localSocketAddress.getPort(), 10, localSocketAddress.getAddress());
                while (!Thread.interrupted()) {
                    try {
                        logger.info("waiting for incoming connection on " + localSocketAddress);
                        final Socket incomingSocket = serverSocket.accept();
                        logger.info("connection accepted from " + incomingSocket.getInetAddress());
                        logger.info("starting port forwarding " + localSocketAddress + " to " + remoteSocketAddress);
                        final PortForwarder portForwarder = new PortForwarder(incomingSocket, remoteSocketAddress);
                        new Thread(portForwarder).start();
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, e.getMessage(), e);
                    }
                }
                logger.info("server stopped");
            } catch (Exception e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        });
        serverThread.start();
    }

    void stop() {
        logger.info("stopping server");
        if (serverThread == null || (serverThread.isAlive() && serverThread.isInterrupted())) {
            logger.severe("already interrupted");
            throw new IllegalStateException("already interrupted");
        }
        serverThread.interrupt();
    }

}
