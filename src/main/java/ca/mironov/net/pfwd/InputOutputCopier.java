package ca.mironov.net.pfwd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputOutputCopier implements Runnable {

    private static final Logger logger = Logger.getLogger(InputOutputCopier.class.getName());

    private final InputStream in;
    private final OutputStream out;
    private final int bufferSize;

    InputOutputCopier(InputStream in, OutputStream out, int bufferSize) {
        this.in = in;
        this.out = out;
        this.bufferSize = bufferSize;
    }

    public void run() {
        try {
            byte[] buffer = new byte[bufferSize];
            while (!Thread.interrupted()) {
                int read = in.read(buffer);
                if (read == -1) {
                    logger.info("input stream has ended");
                    break;
                }
                out.write(buffer, 0, read);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

}
