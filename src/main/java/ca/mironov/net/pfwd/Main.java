package ca.mironov.net.pfwd;

import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        try {
            if (args.length != 2) {
                throw new IllegalArgumentException("Usage: port-forwarder {[local_addr:]local_port} {remote_addr:remote_port}");
            }
            String localArg = args[0];
            InetSocketAddress localSocketAddress = localArg.contains(":") ?
                    new InetSocketAddress(localArg.substring(0, localArg.indexOf(':')), Integer.parseInt(localArg.substring(localArg.indexOf(':') + 1))) :
                    new InetSocketAddress(Integer.parseInt(localArg));
            String remoteArg = args[1];
            InetSocketAddress remoteSocketAddress = new InetSocketAddress(remoteArg.substring(0, remoteArg.indexOf(':')), Integer.parseInt(remoteArg.substring(remoteArg.indexOf(':') + 1)));
            PortForwarderServer server = new PortForwarderServer(localSocketAddress, remoteSocketAddress);
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    server.stop();
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }));
            server.start();
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

}
