package ca.mironov.net.pfwd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PortForwarder implements Runnable {

    private static final Logger logger = Logger.getLogger(PortForwarder.class.getName());

    private final Socket incomingSocket;
    private final InetSocketAddress remoteSocketAddress;

    PortForwarder(Socket incomingSocket, InetSocketAddress remoteSocketAddress) {
        this.incomingSocket = incomingSocket;
        this.remoteSocketAddress = remoteSocketAddress;
    }

    public void run() {
        try {
            logger.info("connecting to " + remoteSocketAddress);
            if (remoteSocketAddress.getAddress() == null) {
                throw new IOException("remote host cannot be resolved: " + remoteSocketAddress);
            }
            final Socket outgoingSocket = new Socket(remoteSocketAddress.getAddress(), remoteSocketAddress.getPort());
            logger.info("connected to " + remoteSocketAddress);

            final Thread inOutThread = createCopierThread(incomingSocket, outgoingSocket);
            final Thread outInThread = createCopierThread(outgoingSocket, incomingSocket);

            inOutThread.start();
            outInThread.start();
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private static Thread createCopierThread(Socket incomingSocket, Socket outgoingSocket) {
        final Thread inOutThread = new Thread(() -> {
            try (InputStream in = incomingSocket.getInputStream()) {
                try (OutputStream out = outgoingSocket.getOutputStream()) {
                    new InputOutputCopier(in, out, 512).run();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        inOutThread.setDaemon(true);
        return inOutThread;
    }

}
